package modele;

import infrastructure.jaxrs.HyperLien;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;

import javax.ws.rs.client.Client;
import java.util.List;
import java.util.Optional;

public class RechercheAsynchroneStreamRx extends RechercheAsynchroneAbstraite {

    private final ImplemNomAlgorithme nomAlgo;

    public RechercheAsynchroneStreamRx(String nom) {
        this.nomAlgo = new ImplemNomAlgorithme(nom);
    }

    @Override
    public Optional<HyperLien<Livre>> chercher(Livre l, List<HyperLien<Bibliotheque>> bibliotheques, Client client) {
        return Observable.fromIterable(bibliotheques)
            .flatMap(
                h -> Observable.fromFuture(this.rechercheAsync(h, l, client))
                    .subscribeOn(Schedulers.io())
            )
            .filter(optHyp -> optHyp.isPresent())
            .blockingFirst(Optional.empty())
        ;
    }

    @Override
    public NomAlgorithme nom() {
        return this.nomAlgo;
    }
}
