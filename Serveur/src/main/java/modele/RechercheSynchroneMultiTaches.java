package modele;

import infrastructure.jaxrs.HyperLien;

import javax.ws.rs.client.Client;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

public class RechercheSynchroneMultiTaches extends RechercheSynchroneAbstraite {

    private final ImplemNomAlgorithme nomAlgo;

    public RechercheSynchroneMultiTaches(String nom) {
        this.nomAlgo = new ImplemNomAlgorithme(nom);
    }


    @Override
    public Optional<HyperLien<Livre>> chercher(Livre l, List<HyperLien<Bibliotheque>> bibliotheques, Client client) {
        ExecutorService service = Executors.newCachedThreadPool();
        CountDownLatch count = new CountDownLatch(bibliotheques.size());
        AtomicReference<Optional<HyperLien<Livre>>> resultAtomicReference = new AtomicReference<>(Optional.empty());

        for (HyperLien<Bibliotheque> hyp : bibliotheques) {
            service.submit(new Runnable() {
                @Override
                public void run() {
                    Optional<HyperLien<Livre>> essai = RechercheSynchroneMultiTaches.this.rechercheSync(hyp, l, client);
                    if (essai.isPresent()) {
                        resultAtomicReference.set(essai);
                    }
                    count.countDown();
                }
            });
        }

        try {
            count.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        service.shutdown();
        return resultAtomicReference.get();
    }

    @Override   
    public NomAlgorithme nom() {
        return this.nomAlgo;
    }
}
