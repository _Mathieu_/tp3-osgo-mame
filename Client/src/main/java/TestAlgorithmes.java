import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class TestAlgorithmes {
    private static final String[] algos = {
            "SynchroneSequentielle",
            "SynchroneMultiTaches",
            "SynchroneStreamParallele",
            "SynchroneStreamRx",
            "AsynchroneSequentielle",
            "AsynchroneMultiTaches",
            "AsynchroneStreamParallele",
            "AsynchroneStreamRx"
    };

    private static String putRequest(URL url, String body) throws IOException {
        HttpURLConnection c = (HttpURLConnection) url.openConnection();
        c.setRequestMethod("PUT");
        c.setRequestProperty("Content-Type", "application/xml");

        // Corps de la requète
        c.setDoOutput(true);
        DataOutputStream os = new DataOutputStream(c.getOutputStream());
        os.writeBytes(body);
        os.close();

        // Exécution et résultat de la requete
        c.connect();
        return "[" + c.getResponseCode() + "] " + c.getResponseMessage();
    }

    public static void main(String[] args) throws IOException {
        // URL init
        String baseUrl = "http://localhost:8081/PortailServeur2/portail/";
        URL urlPortail = new URL(baseUrl);
        URL urlAlgo = new URL(baseUrl + "admin/recherche");

        int[] bibs = new int[25], nums = new int[25];

        for (int i = 0; i < 25; i++) {
            bibs[i] = (int) Math.floor(Math.random() * 10);
            nums[i] = (int) Math.floor(Math.random() * 10);
        }

        for (String nom : algos) {
            putRequest(urlAlgo, "<algo nom=\"Recherche" + nom + "\"/>");
            System.out.println("---\nTest de l'algorithme " + nom);

            long t = System.nanoTime();

            for (int i = 0; i < 25; i++) {
                putRequest(urlPortail, "<livre><titre>Services" + bibs[i] + "." + nums[i] + "</titre></livre>");
                System.out.print("=");
            }

            System.out.println("\nTemps d'exécution moyen : " + (System.nanoTime() - t) / (25 * 1e6) + "ms");
        }
    }
}
